import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './core/header/header.component';
import {FooterComponent} from './core/footer/footer.component';
import {SearchComponent} from './core/search/search.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {SidebarComponent} from './core/sidebar/sidebar.component';
import {FeaturesComponent} from './section/features/features.component';
import {IntroComponent} from './section/intro/intro.component';
import {LocationComponent} from './section/location/location.component';

import {AgmCoreModule} from '@agm/core';
import {CommentComponent} from './section/comment/comment.component';
import {PartnersComponent} from './section/partners/partners.component';
import {CopyrightComponent} from './core/copyright/copyright.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SandboxComponent} from './sandbox/sandbox.component';
import {DatepickerComponent} from './core/search/datepicker/datepicker.component';
import {DropdownComponent} from './core/search/dropdown/dropdown.component';

import {HttpClientModule} from '@angular/common/http';
import {CarouselComponent} from './section/carousel/carousel.component';
import {UserReviewComponent} from './core/user-review/user-review.component';
import {NavBarComponent} from './core/nav-bar/nav-bar.component';
import {AttractionsComponent} from './section/attractions/attractions.component';
import {AttractionComponent} from './section/attractions/attraction/attraction.component';
import {ContactComponent} from './section/contact/contact.component';
import {ContactFormComponent} from './section/contact/contact-form/contact-form.component';
import {AboutComponent} from './section/about/about.component';
import {RoomServiceComponent} from './section/about/room-service/room-service.component';

import {Routes, RouterModule} from '@angular/router';
import { PageHomeComponent } from './page/page-home/page-home.component';
import { PageContactComponent } from './page/page-contact/page-contact.component';
import { PageSuitesComponent } from './page/page-suites/page-suites.component';
import { PageAttractionsComponent } from './page/page-attractions/page-attractions.component';
import { PageAboutComponent } from './page/page-about/page-about.component';
import { SuitesComponent } from './section/suites/suites.component';
import { SuiteComponent } from './section/suites/suite/suite.component';
import { ReviewModalComponent } from './core/user-review/review-modal/review-modal.component';
import {UserReviewService} from './core/user-review/services/user-review.service';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {path: '', component: PageHomeComponent},
  {path: 'suites', component: PageSuitesComponent},
  {path: 'attractions', component: PageAttractionsComponent},
  {path: 'contact', component: PageContactComponent},
  {path: 'about', component: PageAboutComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SearchComponent,
    SidebarComponent,
    FeaturesComponent,
    IntroComponent,
    LocationComponent,
    CommentComponent,
    PartnersComponent,
    CopyrightComponent,
    SandboxComponent,
    DatepickerComponent,
    DropdownComponent,
    CarouselComponent,
    UserReviewComponent,
    NavBarComponent,
    AttractionsComponent,
    AttractionComponent,
    ContactComponent,
    ContactFormComponent,
    AboutComponent,
    RoomServiceComponent,
    PageHomeComponent,
    PageContactComponent,
    PageSuitesComponent,
    PageAttractionsComponent,
    PageAboutComponent,
    SuitesComponent,
    SuiteComponent,
    ReviewModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAxjfmv9sbHI6nsnQcvlKnmls2HwhumDts'
    }),
    NgbModule.forRoot(),
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [UserReviewService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
