import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-location',
  templateUrl: 'location.html',
  styleUrls: ['location-mobile.css', 'location.css']
})
export class LocationComponent implements OnInit {
  lat = -37.8149450;
  lng = 144.9540260;
  constructor() { }

  ngOnInit() {
  }

}
