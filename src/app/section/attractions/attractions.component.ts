import {Component, OnInit} from '@angular/core';
import {CoreService} from '../../core/core.service';
import {Attraction} from './model/Attraction';

@Component({
  selector: 'app-attractions',
  templateUrl: './attractions.component.html',
  styleUrls: ['./attractions.component.css']
})
export class AttractionsComponent implements OnInit {
  attractions: Attraction[];

  constructor(private coreService: CoreService) {
  }

  ngOnInit() {
    this.attractions = this.coreService.fetchAttractions();
  }

  getAttractionClass(index: number): string {
    const isOdd: number = (index + 1) % 2;
    return isOdd === 1 ? 'gray' : 'white';
  }

}
