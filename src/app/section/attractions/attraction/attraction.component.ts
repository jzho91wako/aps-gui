import {Component, Input, OnInit} from '@angular/core';
import {Attraction} from '../model/Attraction';

@Component({
  selector: 'app-attraction',
  templateUrl: 'attraction.html',
  styleUrls: ['attraction-mobile.css', 'attraction-tablet.css', 'attraction.css']
})
export class AttractionComponent implements OnInit {
  @Input() attraction: Attraction;
  @Input() attractionClass: string;
  base = 'assets/img/';
  constructor() { }

  ngOnInit() {
  }

}
