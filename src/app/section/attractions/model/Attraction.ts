export interface Attraction {
  alt: string;
  fileName: string;
  desc: string;
}
