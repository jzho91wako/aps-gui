export interface Comment {
  author: string;
  dateTime?: Date;
  content: string;
}
