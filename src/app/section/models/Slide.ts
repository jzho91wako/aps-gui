export interface Slide {
  class: string;
  title: string;
  text: string;
}
