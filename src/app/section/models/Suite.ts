export interface Suite {
  imgPath: string;
  title: string;
  desc: string;
  features: string[];
  icons: {
    class: string;
    label: string;
  }[];
  price: number;
  hyperLink: string;
}
