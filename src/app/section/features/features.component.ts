import { Component, OnInit } from '@angular/core';

interface Feature {
  name: string;
  icon: string;
}

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {
  features: Feature[];
  constructor() { }

  ngOnInit() {
    this.features = [
      {
        name: 'Free WiFi',
        icon: 'fas fa-wifi'
      },
      {
        name: 'Fully Furnished',
        icon: 'fas fa-bed'
      },
      {
        name: 'Cleaning Services',
        icon: 'fas fa-home'
      },
      {
        name: 'Free Tram Zone',
        icon: 'fas fa-train'
      }
    ];
  }

}
