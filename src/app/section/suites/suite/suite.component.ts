import {Component, Input, OnInit} from '@angular/core';
import {Suite} from '../../models/Suite';

@Component({
  selector: 'app-suite',
  templateUrl: 'suite.html',
  styleUrls: ['suite-mobile.css', 'suite-tablet.css', 'suite.css']
})
export class SuiteComponent implements OnInit {
  @Input() suite: Suite;

  constructor() {
  }

  ngOnInit() {
  }

  onNavigate(link: string) {
    let target = '_blank';
    if (link) {
      target = link;
    }
    window.open(this.suite.hyperLink, target);
  }

}
