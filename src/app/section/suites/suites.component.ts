import {Component, Input, OnInit} from '@angular/core';
import {Suite} from '../models/Suite';
import {CoreService} from '../../core/core.service';

@Component({
  selector: 'app-suites',
  templateUrl: './suites.html',
  styleUrls: ['./suites.css']
})
export class SuitesComponent implements OnInit {
  suites: Suite[];
  constructor(private coreService: CoreService) { }

  ngOnInit() {
    this.suites = this.coreService.fetchSuites();
  }

}
