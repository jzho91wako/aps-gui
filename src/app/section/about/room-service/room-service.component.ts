import {Component, Input, OnInit} from '@angular/core';
import {RoomService} from '../../models/RoomService';

@Component({
  selector: 'app-room-service',
  templateUrl: 'room-service.html',
  styleUrls: ['room-service-mobile.css', 'room-service-tablet.css', 'room-service.css']
})
export class RoomServiceComponent implements OnInit {
  @Input() roomService: RoomService;
  @Input() backgroundClass: string;
  constructor() { }

  ngOnInit() {
  }

}
