import {Component, OnInit} from '@angular/core';
import {CoreService} from '../../core/core.service';
import {RoomService} from '../models/RoomService';

@Component({
  selector: 'app-about',
  templateUrl: 'about.html',
  styleUrls: ['about-mobile.css', 'about-tablet.css', 'about.css']
})
export class AboutComponent implements OnInit {
  roomServices: RoomService[];

  constructor(private coreService: CoreService) {
  }

  ngOnInit() {
    this.roomServices = this.coreService.fetchRoomServices();
  }

  getBackgroundClass(index: number): string {
    const isOdd: number = (index + 1) % 2;
    return isOdd === 1 ? 'gray' : 'white';
  }

}
