import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserReview} from '../../core/models/UserReview';
import {UserReviewService} from '../../core/user-review/services/user-review.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-comment',
  templateUrl: 'comment.html',
  styleUrls: ['comment-mobile.css', 'comment.css']
})
export class CommentComponent implements OnInit, OnDestroy {
  reviews: UserReview[];
  allReviews: UserReview[];
  loadText: string;
  isLoading: boolean;
  updateReviewEventSub: Subscription;

  constructor(private userReviewService: UserReviewService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.loadText = 'See 5 more...';
    this.reviews = [];
    this.allReviews = [];
    this.userReviewService.getAllReviews().subscribe(
      (response) => {
        this.allReviews = <UserReview[]>response;
        if (this.allReviews.length < 5) {
          this.reviews = this.allReviews;
        }
        this.reviews = this.allReviews.slice(0, 5);
        this.isLoading = false;
      }
    );

    this.updateReviewEventSub = this.userReviewService.updateReviewEvent.subscribe(
      (event) => {
        this.updateReviews(event);
      }
    );
  }

  ngOnDestroy() {
    this.updateReviewEventSub.unsubscribe();
  }

  private updateReviews(event: any): void {
    const eventType = event[0],
      eventReview = event[1];
    if (eventType === 'new') {
      this.reviews.unshift(eventReview);
    } else if (eventType === 'update') {
      // update displayed reviews array
      this.reviews = this.reviews.map(
        (r) => {
          if (r.id === eventReview.id) {
            return eventReview;
          }
          return r;
        }
      );

      // update all reviews array
      this.allReviews = this.allReviews.map(
        (r) => {
          if (r.id === eventReview.id) {
            return eventReview;
          }
          return r;
        }
      );
    } else if (eventType === 'delete') {
      // delete review from displayed reviews array
      this.reviews = this.reviews.filter(r => r.id !== eventReview.id);

      // delete review from reviews array
      this.allReviews = this.allReviews.filter(r => r.id !== eventReview.id);
    }
  }

  loadMore(size: number): void {
    const start = this.reviews.length;
    const end = this.reviews.length + size;
    if (this.allReviews.length >= end) {
      this.reviews = this.reviews.concat(this.allReviews.slice(start, end));
    } else {
      this.reviews = this.allReviews;
      this.loadText = 'No more reviews';
    }
  }

  writeReview(): void {
    this.userReviewService.openModalEvent.next(null);
  }

}
