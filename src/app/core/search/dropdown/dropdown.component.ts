import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-core-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {
  @Input() placeholder: string;
  numberOfPeople = 0;

  constructor() {
  }

  ngOnInit() {
  }

  increase(): void {
    if (this.numberOfPeople < 6) {
      this.numberOfPeople++;
    }

  }

  decrease(): void {
    if (this.numberOfPeople > 0) {
      this.numberOfPeople--;
    }
  }

}
