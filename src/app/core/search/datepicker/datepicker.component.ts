import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-core-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit {
  @Input() placeholder: string;
  constructor() { }

  ngOnInit() {
  }

}
