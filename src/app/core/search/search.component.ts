import {Component, OnInit} from '@angular/core';


export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-core-search',
  templateUrl: 'search.html',
  styleUrls: ['search-mobile.css', 'search-tablet.css', 'search.css']
})

export class SearchComponent implements OnInit {
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  constructor() {

  }

  ngOnInit(): void {

  }
}
