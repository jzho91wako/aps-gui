import {Injectable} from '@angular/core';
import {MenuItem} from './models/MenuItem';
import {PaymentMethod} from './models/PaymentMethod';
import {SocialMedia} from './models/SocialMedia';
import {Subject} from 'rxjs';
import {Attraction} from '../section/attractions/model/Attraction';
import {RoomService} from '../section/models/RoomService';
import {Suite} from '../section/models/Suite';

@Injectable({providedIn: 'root'})

export class CoreService {
  toggleMenuEvent: Subject<string> = new Subject<string>();
  private menuItems: MenuItem[] = [
    {
      label: 'Home',
      routerLink: '/',
      mobileOnly: false
    },
    {
      label: 'Apartment & Suites',
      routerLink: '/suites',
      mobileOnly: false
    },
    {
      label: 'Nearby Attractions',
      routerLink: '/attractions',
      mobileOnly: false
    },
    {
      label: 'Sign In',
      routerLink: '/signin',
      mobileOnly: true
    },
    {
      label: 'Contact',
      routerLink: '/contact',
      mobileOnly: false
    },
    {
      label: 'About',
      routerLink: '/about',
      mobileOnly: false
    },
    {
      label: 'Close',
      mobileOnly: true
    }
  ];

  private contactItems: string[] = [
    'cherie@asp.com.au',
    'Tel 046 333 0325'
  ];

  private socialMediaList: SocialMedia[] = [
    {
      name: 'facebook',
      icon: 'fab fa-facebook-f fb',
      tooltip: true
    },
    {
      name: 'twitter',
      icon: 'fab fa-twitter tw',
      tooltip: true
    },
    {
      name: 'Line',
      icon: 'fab fa-line li',
      tooltip: false,
      imgUrl: 'assets/img/qr-wechat.png'
    },
    {
      name: 'Wechat',
      icon: 'fab fa-weixin we',
      tooltip: false,
      imgUrl: 'assets/img/qr-wechat.png'
    }
  ];
  private paymentMethod: PaymentMethod[] = [
    {
      issuer: 'amex',
      icon: 'fab fa-cc-amex',
      tip: 'Please note that there is a 3.0% charge when you pay with a AMEX card.'
    },
    {
      issuer: 'mastercard',
      icon: 'fab fa-cc-mastercard',
      tip: 'Please note that there is a 1.5% charge when you pay with a mastercard.'
    },
    {
      issuer: 'visa',
      icon: 'fab fa-cc-visa',
      tip: 'Please note that there is a 1.5% charge when you pay with a visa card.'
    },
    {
      issuer: 'paypal',
      icon: 'fab fa-cc-paypal',
      tip: ''
    }
  ];
  private address: string[] = [
    '33 Rose Lane',
    'Melbourne CBD',
    'VIC 3000'
  ];

  private attractions: Attraction[] = [
    {
      alt: 'Australian Centre for Moving Images',
      fileName: 'Australian-Centre-for-Moving-Image.jpg',
      desc: 'If you are looking to do fun things in Melbourne for free, then you must visit Australian Centre for Moving Image. ' +
      'It is truly a unique attraction of the city and it hosts shows, films, events, workshops and international exhibitions ' +
      'throughout the year. The main attraction of this centre is the permanent exhibition that documents the journey of moving image. ' +
      'Here you can find equipment which allows you to experiment with your reflection.'
    },
    {
      alt: 'Beach at St Kilda',
      fileName: 'Beach-at-St.-Kilda.jpg',
      desc: 'Beach at St. Kilda is one of the most famous attractions of Melbourne and also one of the top free things to do in the city.' +
      ' This beach is just few miles from city where you can find some great restaurants and bars, ' +
      'you can also find sunbathers and swimmers. ' +
      'St. Kilda is a perfect place for adventurous water- sports such as water- skiing, kite-surfing and sailing.'
    },
    {
      alt: 'City Circle Tram',
      fileName: 'City-Circle-Tram.jpg',
      desc: 'City Circle Tram is a free ride that takes thousands of visitors to the most popular attractions of Melbourne. ' +
      'This Free tram runs every 12 minutes and you can hop into it to visit the best place of interest like Federation Square, ' +
      'Melbourne Aquarium and Parliament House. This is one of the good options among thing to do for free in Melbourne.'
    },
    {
      alt: 'Melbourne Alleys',
      fileName: 'Melbourne-alleys.jpg',
      desc: 'The city centre of Melbourne is quite unique in contrast to its counterparts to the hidden lanes, ' +
      'alleys and arcades to explore. Degraves St, Swanston St, Elizabeth St are some of the places to explore. ' +
      'Besides the breathtaking graffiti, there are cool cafes to relax, some boutiques, shops, bars, and lots of specialty shops. ' +
      'If you are really interested in exploring the place, you can get the map of the arcades and alleys from Visitors ' +
      'Centre in Fed Square.'
    },
    {
      alt: 'Melbourne Festivals',
      fileName: 'Melbourne-Festivals.jpg',
      desc: 'Melbourne is regarded as the festival capital of Australia. Highlights of it are the Fringe festival, ' +
      'Melbourne Music week, Moomba festival, Christmas festival, New Year’s Eve, Good beer week and Next Wave festival ' +
      'are a lot of fun here. The most famous comedy festival catches a lot of free entertainment in Melbourne. ' +
      'Being a part of any of these festivals is so much fun among the free thing to do in Melbourne.'
    },
    {
      alt: 'National Gallery of Melbourne',
      fileName: 'National-Gallery-of-Melbourne.jpg',
      desc: 'National Gallery is located in 180 St. Kilda Road and it is also the first major gallery ' +
      'in the world that is dedicated exclusively to Australian art. The National Galleries of Victoria ' +
      'has two separate sites, first being the NGV International where you can find the Gallery’s International artworks.' +
      'This gallery is a special treat for the art lovers and which is also free among the attraction of Melbourne. ' +
      'This spectacular gallery in itself is a piece of art which is sculptured with perfect cut mirrors of different shapes and colors.'
    },
    {
      alt: 'Queen Victoria Market',
      fileName: 'Queen-Victoria-Market.png',
      desc: 'Since 1878, Queen Victoria Market is one of the city’s top attractions in Melbourne. ' +
      'Here you can stroll around as the stall holders compete with each other to sell their wares. ' +
      'The atmosphere and the settings of this marketplace possess an old world charm that you will ' +
      'not find anywhere else in the city. You can check out this place on Sunday as it provide more ' +
      'relaxed atmosphere than during weekdays.'
    },
    {
      alt: 'River Yarra',
      fileName: 'River-Yarra.jpg',
      desc: 'Take a stroll alongside of the most popular and top free attractions of Melbourne, ' +
      'The Yarra River. A walk at night time along the riverside with the beautifully lit buildings ' +
      'reflecting on the water is simply overwhelming. During the day walk, you can find many unusual ' +
      'venues and works of contemporary arts. If you do not want to miss any artworks then take a look at ' +
      'the River Precinct Artswalk Maps before you leave.'
    }
  ];

  private roomServices: RoomService[] = [
    {
      name: 'Communication and entertainment:',
      services: ['Free WiFi', 'ADSL', 'LCD TV in all the rooms', 'International satellite TV', 'Landline (*paid service)']
    },
    {
      name: 'Comfort and security:',
      services: ['Air conditioner', 'Safe', 'Hairdryer', 'Bed sheets and towels']
    },
    {
      name: 'Kitchen:',
      services: ['Fridge', 'Multifunctional oven with microwave', 'Kettle & coffee maker',
        'Dishwasher', 'Comprehensive set of cutlery, dishes, glasses']
    }
  ];

  private suites: Suite[] = [
    {
      imgPath: 'https://static.wixstatic.com/media/fde015_6c05c2f649564c37a7b5acfd3a848e5b.jpg' +
      '/v1/fill/w_240,h_170,q_85,usm_0.66_1.00_0.01/fde015_6c05c2f649564c37a7b5acfd3a848e5b.jpg',
      title: 'Junior Suite',
      desc: 'Our Junior Suites offer breathtaking views of the city skyline.',
      features: ['Size: 260 sq ft', ' Beds: 2 Double(s)'],
      icons: [{class: 'far fa-snowflake', label: ''},
        {class: 'fas fa-wifi', label: 'Wifi'},
        {class: 'fas fa-tv', label: 'TV'},
        {class: 'fas fa-shower', label: 'Shower'},
        {class: 'fas fa-utensils', label: 'Kitchen'}],
      price: 250,
      hyperLink: ''
    },
    {
      imgPath: 'https://static.wixstatic.com/media/fde015_597c7d9710e44a9d91514e0ac84fb653.jpg' +
      '/v1/fill/w_240,h_170,q_85,usm_0.66_1.00_0.01/fde015_597c7d9710e44a9d91514e0ac84fb653.jpg',
      title: 'Standard Room',
      desc: 'Our Standard Rooms are the perfect combination of function and comfort.',
      features: ['Size: 230 sq ft', 'Beds: 1 Double(s)'],
      icons: [{class: 'far fa-snowflake', label: ''},
        {class: 'fas fa-wifi', label: 'Wifi'},
        {class: 'fas fa-tv', label: 'TV'},
        {class: 'fas fa-shower', label: 'Shower'},
        {class: 'fas fa-utensils', label: 'Kitchen'}],
      price: 150,
      hyperLink: ''
    },
    {
      imgPath: 'https://static.wixstatic.com/media/fde015_6159dcce72024a789a3df21e95e8d495.jpg' +
      '/v1/fill/w_240,h_170,q_85,usm_0.66_1.00_0.01/fde015_6159dcce72024a789a3df21e95e8d495.jpg',
      title: 'Superior Room',
      desc: 'Our Superior Rooms are comfortable, roomy and elegant.',
      features: [' Size: 280 sq ft', 'Beds: 1 King(s)'],
      icons: [{class: 'far fa-snowflake', label: ''},
        {class: 'fas fa-wifi', label: 'Wifi'},
        {class: 'fas fa-tv', label: 'TV'},
        {class: 'fas fa-shower', label: 'Shower'},
        {class: 'fas fa-utensils', label: 'Kitchen'}],
      price: 350,
      hyperLink: ''
    }
  ];

  public fetchMenuItems(): MenuItem[] {
    return this.menuItems.slice();
  }

  public fetchContactItems(): string[] {
    return this.contactItems.slice();
  }

  public fetchSocialMediaList(): SocialMedia[] {
    return this.socialMediaList.slice();
  }

  public fetchPaymentMethod(): PaymentMethod[] {
    return this.paymentMethod.slice();
  }

  public fetchAddress(): string[] {
    return this.address;
  }

  public toggleSideBar(): void {
    this.toggleMenuEvent.next('toggle');
  }

  public fetchAttractions(): Attraction[] {
    return this.attractions.slice();
  }

  public fetchRoomServices(): RoomService[] {
    return this.roomServices.slice();
  }

  public fetchSuites(): Suite[] {
    return this.suites.slice();
  }
}
