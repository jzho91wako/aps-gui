export interface PaymentMethod {
  issuer: 'amex' | 'mastercard' | 'visa' | 'paypal' | 'unionpay';
  icon: string;
  tip: string;
}
