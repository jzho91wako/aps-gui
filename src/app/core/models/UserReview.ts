export interface UserReview {
  id?: string;
  userName: string;
  reviewDate: string;
  comment: string;
  rate: number;
}
