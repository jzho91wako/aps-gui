export interface SocialMedia {
  name: string;
  icon: string;
  tooltip: boolean;
  imgUrl?: string;
}
