export interface MenuItem {
  label: string;
  routerLink?: String;
  mobileOnly: boolean;
}
