import {Component, OnInit} from '@angular/core';
import {CoreService} from '../core.service';

@Component({
  selector: 'app-core-header',
  templateUrl: 'header.html',
  styleUrls: ['header-mobile.css', 'header-tablet.css', 'header.css']
})

export class HeaderComponent implements OnInit {
  constructor(private coreService: CoreService) {

  }

  ngOnInit(): void {
  }

  toggleSideBar(): void {
    this.coreService.toggleSideBar();
  }
}
