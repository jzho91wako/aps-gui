import { Component, OnInit } from '@angular/core';
import {MenuItem} from '../models/MenuItem';
import {CoreService} from '../core.service';

@Component({
  selector: 'app-core-nav-bar',
  templateUrl: 'nav-bar.html',
  styleUrls: ['nav-bar-mobile.css', 'nav-bar.css']
})
export class NavBarComponent implements OnInit {
  menuItems: MenuItem[];
  constructor(private coreService: CoreService) { }

  ngOnInit() {
    this.menuItems = this.coreService.fetchMenuItems().filter( item => !item.mobileOnly );
  }

}
