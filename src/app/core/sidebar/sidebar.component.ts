import {Component, OnInit} from '@angular/core';
import {CoreService} from '../core.service';
import {MenuItem} from '../models/MenuItem';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-core-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: MenuItem[] = [];
  navStyle = 'hide';
  menuTogglerSub: Subscription;

  constructor(private coreService: CoreService) {
  }

  ngOnInit() {
    this.menuItems = this.coreService.fetchMenuItems();
    this.menuTogglerSub = this.coreService.toggleMenuEvent.subscribe(
      (event) => {
        this.toggle();
      }
    );
  }

  toggle(): void {
    this.navStyle === 'show' ? this.navStyle = 'hide' : this.navStyle = 'show';
  }

}
