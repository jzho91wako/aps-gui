import {Observable, Subject} from 'rxjs';
import {UserReview} from '../../models/UserReview';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserReviewService {
  updateReviewEvent: Subject<any[]> = new Subject<any[]>();
  constructor(private http: HttpClient) {
  }

  openModalEvent: Subject<UserReview> = new Subject<UserReview>();

  getAllReviews(): Observable<any> {
    return this.http.get('https://aseyap28v3.execute-api.ap-southeast-2.amazonaws.com/dev/reviews');
  }

  postUserReview(review: UserReview): Observable<any> {
    return this.http.post('https://aseyap28v3.execute-api.ap-southeast-2.amazonaws.com/dev/review', review);
  }

  putUserReview(review: UserReview): Observable<any> {
    return this.http.put('https://aseyap28v3.execute-api.ap-southeast-2.amazonaws.com/dev/review', review);
  }

  deleteUserReview(review: UserReview): Observable<any> {
    return this.http.delete(`https://aseyap28v3.execute-api.ap-southeast-2.amazonaws.com/dev/review/${review.id}`);
  }
}
