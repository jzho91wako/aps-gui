import {Component, Input, OnInit} from '@angular/core';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {UserReview} from '../models/UserReview';
import {UserReviewService} from './services/user-review.service';

@Component({
  selector: 'app-core-user-review',
  templateUrl: 'user-review.html',
  styleUrls: ['user-review-mobile.css', 'user-review.css'],
  providers: [NgbRatingConfig]
})
export class UserReviewComponent implements OnInit {
  @Input() review: UserReview;
  @Input() readonly: boolean;
  isCurrUserAdmin: boolean;
  isUserAuthenticated: boolean;
  responseMessage: string;

  constructor(private config: NgbRatingConfig, private userReviewService: UserReviewService) {
    config.max = 5;
  }

  ngOnInit() {
    this.isCurrUserAdmin = false;
    this.isUserAuthenticated = false;
  }

  getNameInitial(): string {
    let nameInitial = 'UK';
    if (this.review && this.review.userName) {
      const name = this.review.userName.split(' ');
      nameInitial = (name.length > 1 ? name[0][0] + name[1][0] : this.review.userName[0]).toUpperCase();
      nameInitial = nameInitial.length === 1 ? (nameInitial + nameInitial) : nameInitial;
    }
    return nameInitial;
  }

  editReview(): void {
    this.userReviewService.openModalEvent.next(this.review);
  }

  deleteReview(): void {
    this.userReviewService.deleteUserReview(this.review).subscribe(
      (response) => {
        console.log('Success: ' + response.message);
        this.userReviewService.updateReviewEvent.next(['delete', this.review]);
      },
      (error) => {
        console.log('Failed: ' + error);
      }
    );
  }

}
