import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import {UserReviewService} from '../services/user-review.service';
import {Subscription} from 'rxjs';
import {UserReview} from '../../models/UserReview';

@Component({
  selector: 'app-core-review-modal',
  templateUrl: 'review-modal.html',
  styleUrls: ['review-modal.css'],
  providers: [NgbRatingConfig]
})
export class ReviewModalComponent implements OnInit, OnDestroy {
  @ViewChild('content') content: ElementRef;
  closeResult: string;
  openModalEventSub: Subscription;
  title: string;
  review: UserReview;
  reviewText: string;
  rate: number;
  userName: string;
  responseMessage: string;
  alertType: string;

  constructor(private modalService: NgbModal,
              private userReviewService: UserReviewService,
              private config: NgbRatingConfig) {
    config.max = 5;
  }

  ngOnInit() {
    this.title = 'Writing a review...';
    this.openModalEventSub = this.userReviewService.openModalEvent.subscribe(
      (review) => {
        this.review = review;
        this.rate = 0;
        this.userName = '';
        this.reviewText = '';
        if (this.review) {
          this.title = 'Editing a review...';
          this.rate = this.review.rate;
          this.userName = this.review.userName;
          this.reviewText = this.review.comment;
        } else {

        }
        this.open(this.content);
      }
    );
  }

  ngOnDestroy() {
    this.openModalEventSub.unsubscribe();
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private convertDateToString(date: Date): string {
    let dd = String(date.getDate()),
      mm = String(date.getMonth() + 1);
    const yyyy = String(date.getFullYear());
    if (Number(dd) < 10) {
      dd = '0' + dd;
    }

    if (Number(mm) < 10) {
      mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
  }

  onModalDismiss(): void {
    const newReview: UserReview = {
      userName: this.userName,
      reviewDate: this.convertDateToString(new Date),
      comment: this.reviewText,
      rate: this.rate
    };
    // edit mode
    if (this.review) {
      newReview.id = this.review.id;
      this.userReviewService.putUserReview(newReview).subscribe(
        (response) => {
          console.log('Success: ' + response);
          this.userReviewService.updateReviewEvent.next(['update', newReview]);
          this.alertType = 'success';
          this.responseMessage = response.message;
        },
        (error) => {
          console.log('Failed: ' + error);
          this.alertType = 'danger';
          this.responseMessage = error;
        }
      );
    } else {
      this.userReviewService.postUserReview(newReview).subscribe(
        (response) => {
          console.log('Success: ' + response);
          this.userReviewService.updateReviewEvent.next(['new', newReview]);
          this.alertType = 'success';
          this.responseMessage = response.message;
        },

        (error) => {
          console.log('Failed: ' + error);
          this.alertType = 'danger';
          this.responseMessage = error;
        }
      );
    }
  }

}
