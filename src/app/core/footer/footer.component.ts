import {Component, OnInit} from '@angular/core';
import {PaymentMethod} from '../models/PaymentMethod';
import {CoreService} from '../core.service';
import {SocialMedia} from '../models/SocialMedia';

@Component({
  selector: 'app-core-footer',
  templateUrl: 'footer.html',
  styleUrls: ['footer-mobile.css', 'footer.css']
})

export class FooterComponent implements OnInit {
  public paymentMethods: PaymentMethod[];
  public socialMediaItems: SocialMedia[];
  public contactItems: string[];
  public address: string[];

  constructor(private coreService: CoreService) {
  }

  ngOnInit(): void {
    this.paymentMethods = this.coreService.fetchPaymentMethod();
    this.socialMediaItems = this.coreService.fetchSocialMediaList();
    this.contactItems = this.coreService.fetchContactItems();
    this.address = this.coreService.fetchAddress();
  }
}
